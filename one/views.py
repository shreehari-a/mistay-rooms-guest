from django.shortcuts import render
from django.http import HttpResponse
import json

# Create your views here.

def testing(request):
	room = 1
	guest = 1
	return render(request, 'index.html', {"room":room, "guest":guest})


def roomsinc(request):
	rooms = int(request.POST['rooms'])
	guest = int(request.POST['guests'])

	rooms = rooms + 1
	if guest<rooms:
		guest = rooms
	return HttpResponse(json.dumps({'rooms': rooms, 'guest':guest}))

def roomsdec(request):
	rooms = int(request.POST['rooms'])
	guest = int(request.POST['guests'])
	if rooms > 1:
		rooms = rooms - 1
	if guest > 2*rooms:
		guest = 2*rooms
	return HttpResponse(json.dumps({'rooms': rooms, 'guest':guest}))

def guestinc(request):
	rooms = int(request.POST['rooms'])
	guest = int(request.POST['guests'])
	guest = guest + 1
	if guest > rooms*2:
		rooms = rooms+1
	return HttpResponse(json.dumps({'rooms': rooms, 'guest':guest}))

def guestdec(request):
	rooms = int(request.POST['rooms'])
	guest = int(request.POST['guests'])
	
	if guest > 1:
		guest = guest - 1
	if guest < 2*rooms - 1:
		rooms = rooms-1
	return HttpResponse(json.dumps({'rooms': rooms, 'guest':guest}))

